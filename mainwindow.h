#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QPropertyAnimation>
#include <QParallelAnimationGroup>
#include <QGraphicsOpacityEffect>
#include "qfrtest.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private:
  bool eventFilter(QObject *obj, QEvent *e);
  void setSubmitPage(bool display);
  void initAnimation();
  void doTest(int i = 0);

  Ui::MainWindow *ui;
  QFRTest *testAgent;
  QLabel *label_ans[10];
  QPropertyAnimation *animationPool[11];
  QParallelAnimationGroup *hideIntro;
  QGraphicsOpacityEffect *opacityEffect;
  int count, status[10];
};

#endif // MAINWINDOW_H
