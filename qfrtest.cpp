#include <QDir>
#include <QTextStream>
#include "qfrtest.h"

QFRTest::QFRTest(QWidget *parent)
{
    this->parent = parent;
    setWordList(1);
}

void QFRTest::setWordList(int i)
{
    switch (i)
    {
    case 1:
        for (int i = 0; i < 10; i++)
        {
            words[i] = wordList1[i];
        }
        break;
    case 2:
        for (int i = 0; i < 10; i++)
        {
            words[i] = wordList2[i];
        }
        break;
    case 3:
        for (int i = 0; i < 10; i++)
        {
            words[i] = wordList3[i];
        }
        break;
    default:
        for (int i = 0; i < 10; i++)
        {
            words[i] = wordList4[i];
        }
    }
}

QString QFRTest::getWordAt(int i)
{
    return words[i];
}

int QFRTest::getSize()
{
    return size;
}

int QFRTest::searchWord(QString word)
{
    for (int i = 0; i < size; i++)
    {
        if (words[i] == word)
        {
            return i + 1;
        }
    }

    return 0;
}

void QFRTest::generateLog()
{
    QFile file("result.txt");
    file.open(QIODevice::Append);

    if (file.isOpen())
    {
        QTextStream out(&file);
        out.setCodec("utf-8");

        for (int i = 0; i < 10; i++)
        {
            out << sucessCount[i];
            out << (i < 9 ? " " : "\r\n");
        }

        file.close();
    }
}
