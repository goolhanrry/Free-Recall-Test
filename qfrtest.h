#ifndef QFRTEST_H
#define QFRTEST_H

#include <QWidget>
#include <QString>

class QFRTest
{
public:
  explicit QFRTest(QWidget *parent = nullptr);
  void setWordList(int i);
  QString getWordAt(int i);
  int getSize();
  int searchWord(QString word);
  void generateLog();

  int index = 1, sucessCount[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

private:
  QString wordList1[10] = {"桌子", "椅子", "手机", "电脑", "鼠标", "牙刷", "水杯", "相册", "电视", "壁炉"};
  QString wordList2[10] = {"面条", "黑板", "铅笔", "电梯", "包子", "饮料", "台灯", "衣服", "音响", "衣橱"};
  QString wordList3[10] = {"沙发", "插座", "酸奶", "钢琴", "枕头", "键盘", "扫把", "纸巾", "拖鞋", "床单"};
  QString wordList4[10] = {"蜡烛", "灶台", "镜子", "壁画", "空调", "耳机", "水壶", "阳台", "课本", "牙膏"};

  QWidget *parent;
  QString words[10];
  int size = 10;
};

#endif // QFRTEST_H
