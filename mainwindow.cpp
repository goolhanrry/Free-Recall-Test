#include <QTimer>
#include <QSequentialAnimationGroup>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    testAgent = new QFRTest(this);

    ui->setupUi(this);

    this->setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint);
    this->setFixedSize(this->width(), this->height());

    ui->finish->setVisible(false);
    ui->label_thank->setVisible(false);

    label_ans[0] = ui->label_ans_1;
    label_ans[1] = ui->label_ans_2;
    label_ans[2] = ui->label_ans_3;
    label_ans[3] = ui->label_ans_4;
    label_ans[4] = ui->label_ans_5;
    label_ans[5] = ui->label_ans_6;
    label_ans[6] = ui->label_ans_7;
    label_ans[7] = ui->label_ans_8;
    label_ans[8] = ui->label_ans_9;
    label_ans[9] = ui->label_ans_10;

    setSubmitPage(false);

    ui->button_next->installEventFilter(this);
    ui->button_check->installEventFilter(this);
    ui->button_submit->installEventFilter(this);

    this->initAnimation();
}

MainWindow::~MainWindow()
{
    for (int i = 0; i < 10; i++)
    {
        delete animationPool[i];
    }

    delete testAgent;
    delete hideIntro;
    delete opacityEffect;
    delete ui;
}

bool MainWindow::eventFilter(QObject *obj, QEvent *e)
{
    if (obj == ui->button_next && e->type() == QEvent::MouseButtonPress)
    {
        ui->button_next->unsetCursor();
        ui->button_next->removeEventFilter(this);

        hideIntro->start();

        QTimer::singleShot(hideIntro->totalDuration(), [=]() {
            ui->logo->setVisible(false);
            ui->label_info->setVisible(false);
            ui->label_mission->setVisible(false);
            ui->button_next->setVisible(false);

            ui->label_welcome->setText("请尽可能记忆以下词语");
            ui->label_welcome->setFont(QFont("Microsoft YaHei UI", 23, QFont::Normal));
            animationPool[6]->start();

            QTimer::singleShot(2000, [=]() {
                ui->label_word->setGraphicsEffect(opacityEffect);
                doTest();
            });
        });
    }
    else if (obj == ui->button_check && e->type() == QEvent::MouseButtonPress)
    {
        if (ui->lineEdit->text() == "" || count >= 10)
        {
            ui->lineEdit->setText("");
            return false;
        }

        int index = testAgent->searchWord(ui->lineEdit->text());
        if (index && !status[index - 1])
        {
            testAgent->sucessCount[index - 1]++;
            status[index - 1] = 1;
        }

        ui->button_check->removeEventFilter(this);
        ui->label_result->setGeometry(260, 290, 70, 30);
        ui->label_result->setText(ui->lineEdit->text());
        ui->lineEdit->setText("");

        QPropertyAnimation *moveAns = new QPropertyAnimation(ui->label_result, "pos");
        moveAns->setDuration(500);
        moveAns->setStartValue(QPoint(ui->label_result->x(), ui->label_result->y()));
        moveAns->setEndValue(QPoint(count < 5 ? 90 * count + 80 : 90 * count - 370, count < 5 ? 160 : 210));
        moveAns->setEasingCurve(QEasingCurve::OutSine);
        moveAns->start();

        QTimer::singleShot(moveAns->duration() + 200, [=]() {
            label_ans[count]->setText(ui->label_result->text());

            ui->label_result->setText("");
            ui->label_result->setGeometry(260, 7, 70, 30);
            ui->button_check->installEventFilter(this);

            count++;
        });
    }
    else if (obj == ui->button_submit && e->type() == QEvent::MouseButtonPress)
    {
        count = 0;
        animationPool[8]->start();

        QTimer::singleShot(animationPool[8]->duration() + 200, [=]() {
            for (int i = 0; i < 10; i++)
            {
                label_ans[i]->setText("");
            }

            setSubmitPage(false);

            if (testAgent->index >= 4)
            {
                testAgent->generateLog();

                ui->finish->setVisible(true);
                ui->label_thank->setVisible(true);
                animationPool[6]->start();
                return;
            }

            testAgent->index++;
            testAgent->setWordList(testAgent->index);

            ui->label_welcome->setText("请尽可能记忆以下词语");
            ui->label_welcome->setVisible(true);
            ui->label_word->setText("");
            ui->label_word->setVisible(true);
            animationPool[6]->start();

            QTimer::singleShot(2000, [=]() {
                ui->label_word->setGraphicsEffect(opacityEffect);
                doTest();
            });
        });
    }

    return false;
}

void MainWindow::setSubmitPage(bool display)
{
    ui->label_result_title->setVisible(display);
    ui->lineEdit->setVisible(display);
    ui->button_check->setVisible(display);
    ui->button_submit->setVisible(display);

    for (int i = 0; i < 10; i++)
    {
        label_ans[i]->setVisible(display);
    }
}

void MainWindow::initAnimation()
{
    opacityEffect = new QGraphicsOpacityEffect(this);
    opacityEffect->setOpacity(1);

    ui->centralWidget->setGraphicsEffect(opacityEffect);

    animationPool[0] = new QPropertyAnimation(opacityEffect, "opacity");
    animationPool[0]->setDuration(500);
    animationPool[0]->setStartValue(1);
    animationPool[0]->setEndValue(0);

    animationPool[1] = new QPropertyAnimation(ui->logo, "pos");
    animationPool[1]->setDuration(600);
    animationPool[1]->setStartValue(QPoint(ui->logo->x(), ui->logo->y()));
    animationPool[1]->setEndValue(QPoint(ui->logo->x(), ui->logo->y() - 110));
    animationPool[1]->setEasingCurve(QEasingCurve::OutSine);

    animationPool[2] = new QPropertyAnimation(ui->label_welcome, "pos");
    animationPool[2]->setDuration(600);
    animationPool[2]->setStartValue(QPoint(ui->label_welcome->x(), ui->label_welcome->y()));
    animationPool[2]->setEndValue(QPoint(ui->label_welcome->x(), ui->label_welcome->y() - 80));
    animationPool[2]->setEasingCurve(QEasingCurve::OutSine);

    animationPool[3] = new QPropertyAnimation(ui->label_info, "pos");
    animationPool[3]->setDuration(600);
    animationPool[3]->setStartValue(QPoint(ui->label_info->x(), ui->label_info->y()));
    animationPool[3]->setEndValue(QPoint(ui->label_info->x(), ui->label_info->y() - 30));
    animationPool[3]->setEasingCurve(QEasingCurve::OutSine);

    animationPool[4] = new QPropertyAnimation(ui->label_mission, "pos");
    animationPool[4]->setDuration(600);
    animationPool[4]->setStartValue(QPoint(ui->label_mission->x(), ui->label_mission->y()));
    animationPool[4]->setEndValue(QPoint(ui->label_mission->x(), ui->label_mission->y() - 30));
    animationPool[4]->setEasingCurve(QEasingCurve::OutSine);

    animationPool[5] = new QPropertyAnimation(ui->button_next, "pos");
    animationPool[5]->setDuration(600);
    animationPool[5]->setStartValue(QPoint(ui->button_next->x(), ui->button_next->y()));
    animationPool[5]->setEndValue(QPoint(ui->button_next->x() + 40, ui->button_next->y()));
    animationPool[5]->setEasingCurve(QEasingCurve::OutSine);

    animationPool[6] = new QPropertyAnimation(opacityEffect, "opacity");
    animationPool[6]->setDuration(600);
    animationPool[6]->setStartValue(0);
    animationPool[6]->setEndValue(1);

    animationPool[7] = new QPropertyAnimation(opacityEffect, "opacity");
    animationPool[7]->setDuration(500);
    animationPool[7]->setStartValue(0);
    animationPool[7]->setEndValue(1);

    animationPool[8] = new QPropertyAnimation(opacityEffect, "opacity");
    animationPool[8]->setDuration(500);
    animationPool[8]->setStartValue(1);
    animationPool[8]->setEndValue(0);

    animationPool[9] = new QPropertyAnimation(opacityEffect, "opacity");
    animationPool[9]->setDuration(30);
    animationPool[9]->setStartValue(0);
    animationPool[9]->setEndValue(1);

    animationPool[10] = new QPropertyAnimation(opacityEffect, "opacity");
    animationPool[10]->setDuration(30);
    animationPool[10]->setStartValue(1);
    animationPool[10]->setEndValue(0);

    hideIntro = new QParallelAnimationGroup(this);
    for (int i = 0; i < 6; i++)
    {
        hideIntro->addAnimation(animationPool[i]);
    }
}

void MainWindow::doTest(int i)
{
    if (i >= testAgent->getSize())
    {
        ui->label_word->setVisible(false);
        ui->centralWidget->setGraphicsEffect(opacityEffect);
        animationPool[0]->start();

        QTimer::singleShot(animationPool[0]->duration() + 500, [=]() {
            ui->label_welcome->setVisible(false);

            setSubmitPage(true);
            for (int i = 0; i < 10; i++)
            {
                status[i] = 0;
            }

            animationPool[7]->start();
        });
    }
    else
    {
        ui->label_word->setText(testAgent->getWordAt(i));

        QSequentialAnimationGroup *displayWord = new QSequentialAnimationGroup(this);
        displayWord->addAnimation(animationPool[9]);
        displayWord->addPause(3000);
        displayWord->addAnimation(animationPool[10]);
        displayWord->addPause(1000);
        displayWord->start();

        QTimer::singleShot(displayWord->totalDuration() + 200, [=]() mutable {
            doTest(++i);
        });
    }
}
